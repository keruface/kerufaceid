from django.test import TestCase ,Client
from Barang.models import BarangModel
from Barang.forms import BarangForm

# Create your tests here.
class TestBarangViews (TestCase):

    def test_html(self):
        self.client = Client()
        response = self.client.get('/barang/')
        self.assertEqual(response.status_code, 200)

    def test_html_formBarang(self):
        self.client = Client()
        response = self.client.get('/barang/')
        self.assertTemplateUsed(response,"Barang/buatbarang.html")
    
    def test_contain_name (self):
        response = Client().get('/barang/')
        html = response.content.decode('utf8')
        self.assertIn("Upload Product",html)

    def test_else_does_not_exist(self):
        response = Client().get('/antah-berantah/')
        self.assertEqual(response.status_code,404)
    
class TestBarangModel(TestCase):

    def test_buat_barang(self):
        barang_buat = BarangModel(
            brand_barang = "Santuy",
            nama_barang = "bedakSans",
            harga = 30000,
            kategori_barang = "Bedak",
            ukuran_barang = "12 cm",
            terbeli = False,
            gender = "Laki-Laki",
        )
        barang_buat.save()  
        self.assertEqual(BarangModel.objects.all().count(), 1)

    def test_buat_data(self):
        from django.core.files.uploadedfile import SimpleUploadedFile
        small_gif = (
            b'\x47\x49\x46\x38\x39\x61\x01\x00\x01\x00\x00\x00\x00\x21\xf9\x04'
            b'\x01\x0a\x00\x01\x00\x2c\x00\x00\x00\x00\x01\x00\x01\x00\x00\x02'
            b'\x02\x4c\x01\x00\x3b'
        )
        uploaded = SimpleUploadedFile('small.gif', small_gif, content_type='image/gif') 
        data = {
            'brand_barang' : "Santuy",
            'nama_barang' : "bedakSans",
            'harga' : 30000,
            'kategori_barang' : "Bedak",
            'ukuran_barang' : "12 cm",
            'terbeli' : False,
            'gender' : "Laki-Laki",
            'picture' : uploaded,
        }
        response = Client().post("/barang/", data)
        self.assertEqual(response.status_code, 302)


