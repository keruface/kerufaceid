from django.shortcuts import render,redirect,HttpResponseRedirect,HttpResponse
from Barang.forms import BarangForm
from Barang.models import BarangModel


def createBarang(request):
    if request.method == "POST":
        forms= BarangForm(request.POST,request.FILES)
        if forms.is_valid():
            saved = forms.save()
            return redirect("/barang/")
            
    return render(request, "Barang/buatbarang.html", { "data": BarangForm() ,} )

def homepage(request):
    barang_all = BarangModel.objects.filter(terbeli=False)
    return render(request, "Barang/homepage.html", { "barang": barang_all,} )

