from django.urls import path, include
from . import views

app_name = 'barang'
urlpatterns = [
    path('barang/', views.createBarang , name ='createbarang'),
    path('', views.homepage , name = 'homepage'),
]