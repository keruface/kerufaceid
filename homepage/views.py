from django.shortcuts import render, redirect
from .models import Review
from . import forms

def review(request):
    reviews = Review.objects.all().order_by('id')
    return render(request, 'input.html', {'reviews': reviews})

def review_create(request):
    reviews = Review.objects.all().order_by('id')
    if request.method == 'POST':
        form = forms.ReviewForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect('homepage:review_create')

    else:
        form = forms.ReviewForm()
    return render(request, 'input.html', {'form': form, 'reviews':reviews})