from django.db import models
from Barang.models import BarangModel


# Create your models here.
class BarangPembeli(models.Model):
    barang = models.ForeignKey(BarangModel,on_delete=models.CASCADE)
    def __str__(self):
        return self.barang
        