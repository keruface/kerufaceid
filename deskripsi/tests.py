from django.test import TestCase ,Client
from deskripsi.models import BarangPembeli
from Barang.models import BarangModel
from django.test import TestCase, override_settings

@override_settings(STATICFILES_STORAGE='django.contrib.staticfiles.storage.StaticFilesStorage')
# Create your tests here.
class TestBarangViews (TestCase):

    def test_contain_name (self):
        response = Client().get('/deskripsi/semuabarangdibeli/')
        html = response.content.decode('utf8')
        self.assertIn("Barang Terbeli",html)


    def test_else_does_not_exist(self):
        response = Client().get('/error/')
        self.assertEqual(response.status_code,404)
    
class TestBarangModel(TestCase):

    def test_buat_barang(self):
        barang_buat = BarangModel(
            brand_barang = "Santuy",
            nama_barang = "bedakSans",
            harga = 30000,
            kategori_barang = "Bedak",
            ukuran_barang = "12 cm",
            terbeli = False,
            gender = "Laki-Laki",
        )
        barang_buat.save()
        barangkebeli = BarangPembeli (barang=barang_buat)
        barangkebeli.save()
        self.assertEqual(BarangPembeli.objects.all().count(), 1)

