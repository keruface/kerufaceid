from django.urls import path, include
from deskripsi.views import detail_barang,beli_barang,barang_terbeli

urlpatterns = [
    path("konfirmasi/<slug_barang>/" , beli_barang , name= 'barangbeli'),
    path('semuabarangdibeli/' , barang_terbeli , name='barangterbeli'),
    path("<slug_barang>/" , detail_barang , name= 'barangdetail'),
]