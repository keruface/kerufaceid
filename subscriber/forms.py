from django import forms
from .models import Subscriber

class SubscriberForm(forms.ModelForm):

    name = forms.CharField(widget=forms.TextInput(attrs={
        "class" : "form-control",
        "placeholder" : "Name",
    }))
    email = forms.CharField(widget=forms.TextInput(attrs={
        "class" : "form-control",
        "placeholder" : "Email",
    }))

    class Meta:
        model = Subscriber
        fields = ['name', 'email']
