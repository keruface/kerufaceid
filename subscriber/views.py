
from django.shortcuts import render, redirect
from django.http import HttpResponse
from .models import Subscriber
from . import forms


def subscriber(request):
    subscribers = Subscriber.objects.order_by('id')
    return render(request, 'subscriber.html', {'subscribers': subscribers})

def subscriber_create(request):
        if request.method == 'POST':
            form = forms.SubscriberForm(request.POST)
            if form.is_valid():
                form.save()
                return redirect('subscriber:subscriber')

        form = forms.SubscriberForm()
        return render(request, 'koran.html', {'form': form})

