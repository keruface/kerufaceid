from django.test import TestCase

from django.test import TestCase ,Client
from subscriber.models import Subscriber
from subscriber.forms import SubscriberForm
from django.test import TestCase, override_settings


class TestBarangViews (TestCase):

    def test_html(self):
        self.client = Client()
        response = self.client.get('/subscriber/subscriber_create/')
        self.assertEqual(response.status_code, 200)

    def test_html_formBarang(self):
        self.client = Client()
        response = self.client.get('/subscriber/subscriber_create/')
        self.assertTemplateUsed(response,"koran.html")
    
    def test_html_homepage(self):
        self.client = Client()
        response = self.client.get('/subscriber/subscriber/')
        self.assertTemplateUsed(response,"subscriber.html")

    def test_html_homepage1(self):
        self.client = Client()
        response = self.client.get('/subscriber/subscriber/')
        self.assertEqual(response.status_code, 200)
    
    def test_contain_name (self):
        response = Client().get('/subscriber/subscriber/')
        html = response.content.decode('utf8')
        self.assertIn("Subscriber",html)


    def test_else_does_not_exist(self):
        response = Client().get('/rrrrrrrr/')
        self.assertEqual(response.status_code,404)
    
class TestBarangModel(TestCase):
        
    def test_buat_barang(self):
        subscriber_buat = Subscriber(
            name = "bro",
            email = "rani@error.com",
            
        )
        subscriber_buat.save()  
        self.assertEqual(Subscriber.objects.all().count(), 1)

    def test_buat_data(self):
        data = {
            'name' : "bro",
            'email' : "rani@error.com",
        }
        response = Client().post("/subscriber/subscriber_create/", data)
        self.assertEqual(response.status_code, 302)



