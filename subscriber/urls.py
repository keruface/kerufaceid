from django.urls import path
from . import views

app_name = 'subscriber'
urlpatterns = [
    path('subscriber_create/',views.subscriber_create,name='subscriber_create'),
    path('subscriber/',views.subscriber,name='subscriber'),
]
